-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2017 at 05:47 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `palakkunnel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `billid` varchar(50) NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(9,2) UNSIGNED NOT NULL,
  `totalprice` decimal(9,2) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'food items');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `quantity` decimal(9,2) UNSIGNED DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `quantity`, `category_id`, `created`, `modified`) VALUES
(1, 'rice', '348.00', '150.00', 1, '2017-04-06 20:59:55', '2017-04-06 21:09:17'),
(2, 'oil', '95.00', '20.00', 1, '2017-04-06 21:00:23', '2017-04-06 21:09:37'),
(3, 'fish', '60.00', '15.00', 1, '2017-04-06 21:01:05', '2017-04-06 21:09:57'),
(4, 'chilly powder', '30.00', '5.00', 1, '2017-04-06 21:01:33', '2017-04-06 21:10:10'),
(5, 'cake 1kg', '170.00', '15.00', 1, '2017-04-06 21:02:12', '2017-04-06 21:10:19'),
(6, 'pepsi 500ml', '35.00', '35.00', 1, '2017-04-06 21:02:48', '2017-04-06 21:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`) VALUES
(1, 'saji', '$2y$10$1CJfYb0y5OXOBUWjIMpd4O6t54L46aXjmPMzZrOFSaW/Wv4rPBWkm', 'admin', '2017-04-03 16:52:28', '2017-04-06 18:35:20'),
(2, 'user', '$2y$10$KdwSlItqo1r.1szfKXuF4OVv46aseLllx4KQBtGo9tmOwR5evzxxK', 'staff', '2017-04-03 16:52:46', '2017-04-06 18:35:34'),
(3, 'unni', '$2y$10$Maim5bv96J2FGuIj5Qw9cenRwQOvVJZpsrcg4nANBaG8D0OVPXVpG', 'admin', '2017-04-03 17:05:06', '2017-04-03 17:05:06'),
(4, 'sneha', '$2y$10$KgCYzrWxmPLa/uBQ1SFCG.BnO3lQ7esqyb9rgR96pNtUilCzPX3U6', 'admin', '2017-04-04 10:43:57', '2017-04-06 18:38:35'),
(5, 'testemployee', '$2y$10$VvTIn25z.chJDUZD3zdSruCwUZmdEDgtQMmGVRMZsD5vzYazz8Pou', 'staff', '2017-04-06 18:43:49', '2017-04-06 18:43:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkbillsitems` (`item_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkcategoryitems` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `fkbillsitems` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fkcategoryitems` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

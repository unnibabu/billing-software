<!-- File: src/Template/Users/login.ctp -->
<div class="users form">
    <?= $this->Flash->render() ?>
    <form method="post" action="<?= $this->Url->build(["controller" => "Users", "action" => "login"]) ?>">
        <center><h2>Palakunnel Traders</h2></center>
        <div style="width: 40%;margin: auto;box-shadow: 3px 3px 18px grey;padding: 20px;">
            <fieldset>
                <legend><?= __('Please enter your username and password') ?></legend>
                <?= $this->Form->control('username') ?>
                <?= $this->Form->control('password') ?>
                <?= $this->Form->button(__('Login')); ?>
            </fieldset>
        </div>

    </form>
</div>
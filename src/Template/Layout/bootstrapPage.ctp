<?php
    /**
     * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
     * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
     * Licensed under The MIT License
     * For full copyright and license information, please see the LICENSE.txt
     * Redistributions of files must retain the above copyright notice.
     * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
     * @link          http://cakephp.org CakePHP(tm) Project
     * @since         0.10.0
     * @license       http://www.opensource.org/licenses/mit-license.php MIT License
     */
    
    $cakeDescription = 'Palakunnel Traders';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?php echo $this->Html->css("bootstrap.css"); ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?php echo $this->Html->css("fa/css/font-awesome.min.css"); ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<nav class="top-bar expanded" data-topbar role="navigation">
    <ul class="title-area large-3 medium-4 columns">
        <li class="name">
            <h1><a tabindex="-1" href=""><?= $this->fetch('title') ?></a></h1>
        </li>
    </ul>
    <div class="top-bar-section">
        <ul class="right">
            <?php
                if (isset($_SESSION["Auth"]["User"])) {
                    if ($_SESSION["Auth"]["User"]["role"] == "admin") {
                        ?>
                        <li><a tabindex="-1"
                               href="<?php echo $this->Url->build(["controller" => "Admin", "action" => "index"]); ?>">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                Admin</a></li>
                        <li><a tabindex="-1"
                               href="<?php echo $this->Url->build(["controller" => "Users", "action" => "index"]); ?>">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                Users</a></li>
                        <?php
                    }
                }
            ?>
            <li><a tabindex="-1"
                   href="<?php echo $this->Url->build(["controller" => "Categories", "action" => "index"]); ?>">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Categories</a></li>
            <li><a tabindex="-1"
                   href="<?php echo $this->Url->build(["controller" => "Bills", "action" => "index"]); ?>">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    Bill View</a></li>
            <li><a tabindex="-1"
                   href="<?php echo $this->Url->build(["controller" => "Items", "action" => "index"]); ?>">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    All Items</a>
            </li>
            <li><a href="<?php echo $this->Url->build(["controller" => "Users", "action" => "logout"]); ?>"><i
                            class="fa fa-user" aria-hidden="true"></i>
                    Logout</a></li>
        </ul>
    </div>
</nav>
<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>
<footer>
</footer>
</body>
</html>

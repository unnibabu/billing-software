<?php
    /**
     * @var \App\View\AppView $this
     */
?>
<?php
    echo $this->Html->script("jquery.min.js");
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bills index large-9 medium-8 columns content">
    <h3><?= __('Bills') ?></h3>
    <input type="text" id="search" placeholder="Search..." style="width: 60%;float: left;"/>
    <input onclick="searchme()" type="button" value="Search"
           style="float: left;margin-left: 10px;width: 100px;height: 40px;background: #116D76;color: white;"/>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('billid') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($bills as $bill): ?>
            <tr>
                <td><?= h(str_replace("Bill ID :", "", $bill->billid)) ?></td>
                <td><?= h($bill->modified->timeAgoInWords()) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'billview', $bill->billid]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'billfulldelete', $bill->billid], ['confirm' => __('Are you sure you want to delete # {0}?', $bill->billid)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script>
    function searchme() {
        if ($("#search").val().length < 2) {
            alert("Please add Search term");
            return false;
        }

        var searchlink = "<?php echo $this->Url->build([
            "controller" => "Bills",
            "action"     => "Search"
        ]); ?>";

        parent.self.location = searchlink + "?query=" + $("#search").val();


    }

    $("#search").keypress(function (e) {
        if (e.which == 13) {
            searchme();
        }
    });
</script>

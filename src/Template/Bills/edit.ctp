<?php
    /**
     * @var \App\View\AppView $this
     */
    echo $this->Html->script("jquery.min.js");
    echo $this->Html->css("select2.min.css");
    echo $this->Html->script("select2.full.min.js");
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bill->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('List Bills'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bills form large-9 medium-8 columns content">
    
    <?= $this->Form->create($bill) ?>
    <fieldset>
        <legend><?= __('Edit Bill Item') ?></legend>
        <?php
            echo $this->Form->input('billid', ["readonly" => "true", "type" => "hidden"]);
        ?>
        <h6>Bill ID : <?php echo $bill->billid; ?></h6>
        <h6>Item : <?php echo $bill->item->name; ?></h6>
        <br><br>
        <?php
            echo $this->Form->input('quantity', ["id" => "quantity", "onchange" => "pricechange()"]);
            echo $this->Form->input('totalprice', ["readonly" => "true", "id" => "totalprice"]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<script>
    var itemprice = "<?php echo $bill->item->price; ?>";


    function pricechange() {
        var quantity = $("#quantity").val();
        $("#totalprice").val((itemprice * quantity).toFixed(2));
    }
</script>

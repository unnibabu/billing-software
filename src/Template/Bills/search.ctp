<?php
    /**
     * @var \App\View\AppView $this
     */
?>
<?php
    echo $this->Html->script("jquery.min.js");
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Item'), ["controller" => "Items", 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="items index large-10 medium-9 columns content">
    <h3><?= __('Bill Search') ?></h3>

    <input type="text" id="search" placeholder="Search..." style="width: 60%;float: left;"/>
    <input onclick="searchme()" type="button" value="Search"
           style="float: left;margin-left: 10px;width: 100px;height: 40px;background: #116D76;color: white;"/>

    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('billid') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($bills as $billitem): ?>
            <tr>
                <td><?= h($billitem->billid) ?></td>
                <td><?= h($billitem->modified->timeAgoInWords()) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'billview', $billitem->billid]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'billfulldelete', $billitem->billid], ['confirm' => __('Are you sure you want to delete # {0}?', $billitem->billid)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<?php
    if (isset($_GET["query"])) {
        if (strlen($_GET["query"]) > 0) {
            ?>
            <script>
                $("#search").val("<?php echo $_GET["query"] ?>");
            </script>
            <?php
            
        }
    }
?>

<script>
    function searchme() {
        if ($("#search").val().length < 2) {
            alert("Please add Search term");
            return false;
        }

        var searchlink = "<?php echo $this->Url->build([
            "controller" => "Bills",
            "action"     => "Search"
        ]); ?>";

        parent.self.location = searchlink + "?query=" + $("#search").val();


    }


    $("#search").keypress(function (e) {
        if (e.which == 13) {
            searchme();
        }
    });
</script>

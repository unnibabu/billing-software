<?php
    echo $this->Html->script("jquery.min.js");
    echo $this->Html->script("jQuery.print.js");
?>
<style>
    #onlyprint {
        display: none;
    }
</style>
<center><h3>Bill Details</h3>
    <h5 id="billiddiv">Bill ID : <span id="billnumberid"><?php echo $billid; ?></span></h5>
    <h6>Date : <?php echo date("d-m-Y H:i:s", strtotime($billdate->created)); ?>
</center>
</h6>
<table>
    <thead>
    <tr>
        <td>Item Name</td>
        <td>Quantity</td>
        <td>Rate+Vat</td>
        <td>Price</td>
        <td>Actions</td>
    </tr>
    </thead>
    <tbody>
    <?php
        foreach ($billdata as $singlebill) {
            echo "<tr>";
            echo "<td class='itemselect'>" . $singlebill->item->name . "</td>";
            echo "<td class='qtyitems'>" . $singlebill->quantity . "</td>";
            echo "<td class='ratevat'>" . sprintf("%.2f", $singlebill->item->price) . "</td>";
            echo "<td class='pricehere'>" . sprintf("%.2f", $singlebill->totalprice) . "</td>";
            echo "<td>";
            echo $this->Html->link(__('Edit'), ['action' => 'edit', $singlebill->id]) . "  ";
            echo "   " . $this->Form->postLink(__('Delete'), ['action' => 'delete', $singlebill->id], ['confirm' => __('Are you sure you want to delete # {0}?', $singlebill->id)]);
            echo "</td>";
            echo "</tr>";
        }
    ?>
    </tbody>
</table>
<h3 class="pull-right" id="totalprice">Total : </h3>
<br><br>
<center>
    <button onclick="printme()">PRINT</button>
</center>
<div id="onlyprint">
    <span style="font-family:monospace;font-size: 16px;">PALAKUNNEL TRADERS</span>
    <span style="font-size:14px;font-family:monospace">
        (Phone : 9961823274)
    </span><br>
    <span style="font-size:14px;font-family:monospace" id="billidprintdisplay"></span>
    <span style="font-size:13px;font-family:monospace" id="datebilldisplay" class="pull-right"></span>
    <div>
        <table id="printtable">
            <tbody id="printbody">

            </tbody>
        </table>
    </div>
    <span id="totalprinterdisplay" style="font-weight:700;font-family:monospace"></span>
</div>


<script>
    getTotalPrice();
    function getTotalPrice() {
        var totalprice = 0;
        $(".pricehere").each(function () {
            totalprice += parseFloat($(this).html());
        });
        $("#totalprice").html("Total " + totalprice.toFixed(2));
        $("#loadergif").css({"display": "none"});
    }


    function printme() {
        var printhtml = "<tr>" +
            "<th style='font-weight:bold;font-size:13px;font-family:monospace'>No</th>" +
            "<th style='font-weight:bold;font-size:13px;font-family:monospace;text-align: center;'>Item</th>" +
            "<th style='font-weight:bold;font-size:13px;font-family:monospace'>Qty</th>" +
            "<th style='font-weight:bold;font-size:13px;font-family:monospace'>Rate+VAT</th>" +
            "<th style='font-weight:bold;font-size:13px;text-align: center;font-family:monospace'>Price</th>" +
            "</tr>";
        var counter = 1;
        $(".itemselect ").each(function () {
            console.log($(this).parent().find(".itemselect"));
            printhtml += "<tr>";
            printhtml += "<td style='font-weight:bold;font-size:11px;text-align: center;font-family:monospace'>" + counter + "</td>";
            printhtml += "<td style='font-weight:bold;font-size:11px;text-align: left;width:50%;font-family:monospace'>" + $(this).parent().find(".itemselect").text() + "</td>";
            printhtml += "<td style='font-weight:bold;font-size:11px;text-align: center;font-family:monospace'>" + $(this).parent().find(".qtyitems").text() + "</td>";
            printhtml += "<td style='font-weight:bold;font-size:11px;text-align: center;width:10%;font-family:monospace'>" + $(this).parent().find(".ratevat").text() + "</td>";
            printhtml += "<td style='font-weight:bold;font-size:11px;text-align: center;font-family:monospace'>" + $(this).parent().find(".pricehere").text() + "</td>";
            printhtml += "</tr>";
            counter++;
        });

        $("#printbody").html(printhtml);
        $("#totalprinterdisplay").html($("#totalprice").text());
        $("#billidprintdisplay").html("Bill ID " + $("#billnumberid").text());
        var billdate = new Date();
        $("#datebilldisplay").html(billdate.getDate() + "/" + (billdate.getMonth() + 1) + "/" + billdate.getFullYear() + " " + billdate.getHours() + ":" + billdate.getMinutes() + ":" + billdate.getSeconds());
        $("#onlyprint").print({
            globalStyles: false,
            mediaPrint: false,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 750,
            title: null,
            doctype: '<!doctype html>'
        });
    }
</script>
<?php
    /**
     * @var \App\View\AppView $this
     */
?>

<?php
    echo $this->Html->css("select2.min.css");
    echo $this->Html->script("jquery.min.js");
    echo $this->Html->script("select2.full.min.js");
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="items form large-9 medium-8 columns content">
    <?= $this->Form->create($item) ?>
    <fieldset>
        <legend><?= __('Add Item') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('price');
            echo $this->Form->input('quantity');
        ?>

        <label for="categoryid"><b>Category</b></label>
        <select name="category_id" id="categoryid">
            <?php
                foreach ($category as $singleCategory) {
                    echo "<option value='$singleCategory->id'>" . $singleCategory->name . "</option>";
                }
            ?>
        </select>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<script>
    $("#categoryid").select2();
</script>

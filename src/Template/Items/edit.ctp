<?php
    /**
     * @var \App\View\AppView $this
     */
?>
<?php
    echo $this->Html->css("select2.min.css");
    echo $this->Html->script("jquery.min.js");
    echo $this->Html->script("select2.full.min.js");
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $item->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="items form large-9 medium-8 columns content">
    <?= $this->Form->create($item) ?>
    <fieldset>
        <legend><?= __('Edit Item') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('price');
            echo $this->Form->input('quantity');
        ?>

        <label><b>Category</b></label>
        <select id="categoryid" name="category_id">
            <?php
                foreach ($categories as $singlecategory) {
                    echo "<option value='$singlecategory->id'>$singlecategory->name</option>";
                }
            ?>
        </select>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<script>
    var catergoryid = "<?php echo $item->category_id; ?>";
    $("#categoryid").val(catergoryid);
    $("#categoryid").select2();
</script>

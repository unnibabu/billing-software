<?php
    /**
     * @var \App\View\AppView $this
     */
?>
<?php
    echo $this->Html->script("jquery.min.js");
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?></li>
        <li>
            <a target="_blank" href="<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'export']); ?>">
                <span class="fa fa-download"> </span>
                <b>BACKUP ITEMS</b>
            </a>

        </li>
    </ul>
</nav>
<div class="items index large-10 medium-9 columns content">
    <h3><?= __('Items') ?></h3>

    <input type="text" id="search" placeholder="Search..." style="width: 60%;float: left;"/>
    <input onclick="searchme()" type="button" value="Search"
           style="float: left;margin-left: 10px;width: 100px;height: 40px;background: #116D76;color: white;"/>

    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
            <th scope="col">&#8377;<?= $this->Paginator->sort('price') ?></th>
            <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
            <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item): ?>
            <tr>
                <td><b><?= h($item->name) ?></b></td>
                <td>&#8377; <?= $this->Number->format($item->price) ?></td>
                <td><?= $this->Number->format($item->quantity) ?></td>
                <td><?= $item->category->name ?></td>
                <td><?= h($item->modified->timeAgoInWords()) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $item->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $item->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<script>
    function searchme() {
        if ($("#search").val().length < 2) {
            alert("Please add Search term");
            return false;
        }

        var searchlink = "<?php echo $this->Url->build([
            "controller" => "Items",
            "action"     => "Search"
        ]); ?>";

        parent.self.location = searchlink + "?query=" + $("#search").val();


    }

    $("#search").keypress(function (e) {
        if (e.which == 13) {
            searchme();
        }
    });
</script>

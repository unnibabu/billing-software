<style>
    .sections {
        box-shadow: 2px 2px 20px grey;
        height: auto;
        width: 45%;
        float: left;
        margin: 5px;
        padding: 20px;
        font-size: 20px;
    }
</style>

<div class="row">
    <div class="col-md-6 sections">
        <h4><u>Bills</u></h4>
        Bills Total : <?= $billcount; ?><br> (Total Sales Rs. <?php echo $totalsales["totalsales"]; ?>)
        <br> <br>
        Bills this month : <?= $billthismonth; ?><br> (Sales This Month Rs. <?php echo $salesmonth["salesmonth"]; ?>)
        <br> <br>
        Bills Today : <?= $billtoday; ?><br>
        (Sales Today : Rs.<?php echo $salestoday->salestoday; ?>)
        <br> <br>
        <a href="<?php echo $this->Url->build(["controller" => "Bills", "action" => "index"]) ?>">
            <button class="btn btn-primary">VIEW BILLS</button>
        </a>
    </div>
    <div class="col-md-6 sections">
        <h4><u>Items</u></h4>
        Items Total : <?= $itemcount; ?>
        <br>
        Out of Stock : <?= $outofstock; ?>
        <br> <br>
        <a href="<?php echo $this->Url->build(["controller" => "Items", "action" => "index"]) ?>">
            <button class="btn btn-primary">VIEW ITEMS</button>
        </a>
    </div>
</div>
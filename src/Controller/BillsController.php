<?php
    namespace App\Controller;
    
    use App\Controller\AppController;
    use Cake\Log\Log;
    use Cake\ORM\TableRegistry;
    use Symfony\Component\Debug\Debug;

    /**
     * Bills Controller
     * @property \App\Model\Table\BillsTable $Bills
     */
    class BillsController extends AppController
    {
        
        /**
         * Index method
         * @return \Cake\Network\Response|null
         */
        public function index()
        {
            $this->paginate = [
                'contain' => ['Items']
            ];
            $bills = $this->paginate($this->Bills->find()->group(["billid"]),
                [
                    'order' => [
                        'Bills.modified' => 'desc'
                    ]
                ]
            );
            
            $this->set(compact('bills'));
            $this->set('_serialize', ['bills']);
        }
        
        /**
         * View method
         * @param string|null $id Bill id.
         * @return \Cake\Network\Response|null
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function view($id = null)
        {
            $bill = $this->Bills->get($id, [
                'contain' => ['Items']
            ]);
            
            $this->set('bill', $bill);
            $this->set('_serialize', ['bill']);
        }
        
        /**
         * Add method
         * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
         */
        public function add()
        {
            if ($this->request->is('ajax')) {
                $billdata = $this->request->data["item"];
                $bill = TableRegistry::get("Bills");
                $entity = $bill->newEntities($billdata);
                $bill->saveMany($entity);
                // decrement quantity of items from stock
                $itementity = TableRegistry::get("Items");
                for ($i = 0; $i < count($billdata); $i++) {
                    $particularItem = $itementity->get($billdata[$i]["item_id"]);
                    $particularItem->quantity = $particularItem->quantity - $billdata[$i]["quantity"];
                    $itementity->save($particularItem);
                }
                $this->autoRender = false;
                return null;
            }
    
    
            $testbill = TableRegistry::get("Items")->find()->select(["id", "value" => "name", "label" => "name", "price"])->all();
            $this->set(compact('testbill', $testbill));
    
    
            $this->set('_serialize', ['bill']);
            $this->viewBuilder()->layout("bootstrapPage");
        }
        
        /**
         * Edit method
         * @param string|null $id Bill id.
         * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
         * @throws \Cake\Network\Exception\NotFoundException When record not found.
         */
        public function edit($id = null)
        {
            $bill = $this->Bills->get($id, [
                'contain' => ["Items"]
            ]);
    
    
            if ($this->request->is(['patch', 'post', 'put'])) {
                $oldquantity = $bill->quantity;
                $bill = $this->Bills->patchEntity($bill, $this->request->data);
                if ($oldquantity > $bill->quantity)  //if old quantity > new quantity,add to stock
                {
                    $itemsTable = TableRegistry::get("Items");
                    $itemsentity = $itemsTable->get($bill->item_id);
                    $itemsentity->quantity = $itemsentity->quantity + ($oldquantity - $bill->quantity);
                    $itemsTable->save($itemsentity);
                } else if ($oldquantity < $bill->quantity)//if old quantity > new quantity,subtract from stock
                {
                    $itemsTable = TableRegistry::get("Items");
                    $itemsentity = $itemsTable->get($bill->item_id);
                    $itemsentity->quantity = $itemsentity->quantity - ($bill->quantity - $oldquantity);
                    $itemsTable->save($itemsentity);
                }
                if ($this->Bills->save($bill)) {
                    $this->Flash->success(__('The bill has been saved.'));
    
                    return $this->redirect(['action' => 'billview', $bill->billid]);
                }
                $this->Flash->error(__('The bill could not be saved. Please, try again.'));
            }
    
            $this->set('bill', $bill);
           
        }
        
        /**
         * Delete method
         * @param string|null $id Bill id.
         * @return \Cake\Network\Response|null Redirects to index.
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function delete($id = null)
        {
            $this->request->allowMethod(['post', 'delete']);
            $bill = $this->Bills->get($id);
    
            //========== Add bill quantity to stock, restore it
            $itemsTable = TableRegistry::get("Items");
            $itemsentity = $itemsTable->get($bill->item_id);
            $itemsentity->quantity = $itemsentity->quantity + $bill->quantity;
            $itemsTable->save($itemsentity);
            
            if ($this->Bills->delete($bill)) {
                $this->Flash->success(__('The bill has been deleted.'));
            } else {
                $this->Flash->error(__('The bill could not be deleted. Please, try again.'));
            }
            
            return $this->redirect(['action' => 'index']);
        }
    
    
        public function billfulldelete($id = null)
        {
            $bills = $this->Bills->find()->where(["billid" => $id])->all();
            foreach ($bills as $singlebill) {
                $itemsTable = TableRegistry::get("Items");
                $itemsentity = $itemsTable->get($singlebill->item_id);
                $itemsentity->quantity = $itemsentity->quantity + $singlebill->quantity;
                $itemsTable->save($itemsentity);
            }
            if ($this->Bills->deleteAll(["billid" => $id])) {
                $this->Flash->success(__('The bill has been deleted.'));
            } else {
                $this->Flash->error(__('The bill could not be deleted. Please, try again.'));
            }
        
            return $this->redirect(['action' => 'index']);
        }
    
    
        public function search()
        {
            $query = $_GET["query"];
            $searchresult = TableRegistry::get("Bills")->find()->where(
                ["Bills.billid LIKE" => '%' . $query . '%']
            )->group(["Bills.billid"]);
            $this->paginate($searchresult,
                ['contain'  => ['Items'],
                 'maxLimit' => 20
                ]);
            $this->set("bills", $searchresult);
        }
    
    
        public function billview($billid = null)
        {
            $billcreated = $this->Bills->find()->where([
                "billid" => $billid
            ])->contain(["Items"])->select(["created", "modified"])->first();
        
        
            $bill = $this->Bills->find()->where([
                "billid" => $billid
            ])->contain(["Items"])->all();
            $this->set("billid", $billid);
            $this->set("billdata", $bill);
            $this->set("billdate", $billcreated);
        }
        
    }

<?php
    namespace App\Controller;
    
    use App\Controller\AppController;
    use Cake\Log\Log;
    use Cake\ORM\TableRegistry;
    use Cake\I18n\Number;
    /**
     * Items Controller
     * @property \App\Model\Table\ItemsTable $Items
     */
    class ItemsController extends AppController
    {
        
        /**
         * Index method
         * @return \Cake\Network\Response|null
         */
        public function index()
        {
            $items = $this->paginate($this->Items,
                ['contain'  => ['Categories'],
                 'maxLimit' => 7,
                 'order'    => [
                     'Items.modified' => 'desc'
                 ]
                ]);
            
            $this->set(compact('items'));
            $this->set('_serialize', ['items']);
        }
        
        /**
         * View method
         * @param string|null $id Item id.
         * @return \Cake\Network\Response|null
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function view($id = null)
        {
            $item = $this->Items->get($id, [
                'contain' => ["Categories"]
            ]);
            
            $this->set('item', $item);
            $this->set('_serialize', ['item']);
        }
        
        /**
         * Add method
         * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
         */
        public function add()
        {
            $item = $this->Items->newEntity();
            if ($this->request->is('post')) {
                $this->request->data["name"] = preg_replace("/[^a-zA-Z0-9 ]+/", "", $this->request->data["name"]);
                $item = $this->Items->patchEntity($item, $this->request->data);
                if ($this->Items->save($item)) {
                    $this->Flash->success(__('The item has been saved.'));
    
                    return $this->redirect(['action' => 'add']);
                }
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
            $this->set(compact('item'));
            $this->set('_serialize', ['item']);
            
            $categories = TableRegistry::get("Categories")->find();
            $this->set("category", $categories);
            
        }
        
        /**
         * Edit method
         * @param string|null $id Item id.
         * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
         * @throws \Cake\Network\Exception\NotFoundException When record not found.
         */
        public function edit($id = null)
        {
            $item = $this->Items->get($id, [
                'contain' => []
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $this->request->data["name"] = preg_replace("/[^a-zA-Z0-9 ]+/", "", $this->request->data["name"]);
                $item = $this->Items->patchEntity($item, $this->request->data);
                if ($this->Items->save($item)) {
                    $this->Flash->success(__('The item has been saved.'));
                    
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
            
            
            $categories = TableRegistry::get("Categories")->find();
            $this->set(compact('item'));
            $this->set('_serialize', ['item']);
            $this->set("categories", $categories);
        }
        
        /**
         * Delete method
         * @param string|null $id Item id.
         * @return \Cake\Network\Response|null Redirects to index.
         * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
         */
        public function delete($id = null)
        {
            $this->request->allowMethod(['post', 'delete']);
            $item = $this->Items->get($id);
            if ($this->Items->delete($item)) {
                $this->Flash->success(__('The item has been deleted.'));
            } else {
                $this->Flash->error(__('The item could not be deleted. Please, try again.'));
            }
            
            return $this->redirect(['action' => 'index']);
        }
    
    
        public function search()
        {
            $query = $_GET["query"];
            $searchresult = TableRegistry::get("Items")->find()->contain(["Categories"])
                ->where([
                        "OR" => [["Categories.name LIKE" => '%' . $query . '%'], ["Items.name LIKE" => '%' . $query . '%']]
                    ]
            );
            $this->paginate($searchresult,
                [
                 'maxLimit' => 50
                ]);
            $this->set("items", $searchresult);
        }
    
        public function getPrice()
        {
            if ($this->request->is("ajax")) {
                $itemprice = TableRegistry::get("Items")->find()->select("price")->where([
                    "id" => $this->request->data["itemid"]
                ])->first();
            
                $quantity = (int)$this->request->data["qty"];
                $totalprice = $itemprice->price * $quantity;
                $formattedTotal = Number::precision($totalprice, 2);
                $this->response->body(str_replace(",", "", $formattedTotal));
                return $this->response;
            }
        
            $this->autoRender = false;
        }
    
    
        public function populate()
        {
            ini_set('max_execution_time', 900); //300 seconds = 5 minutes
            $id = 1;
            for ($i = 0; $i < 50000; $i++) {
                $item = $this->Items->newEntity();
                $item->name = "item" . $id;
                $item->price = (10.00 + lcg_value() * (abs(5000.00 - 10.00)));
                $item->category_id = 2;
                $item->quantity = rand(1, 150);
                $insertid = $this->Items->save($item);
                $id = $insertid->id + 1;
            }
            echo "success";
            $this->autoRender = false;
    
        }
    
    
        // View used will be in src/Template/Posts/csv/export.ctp
        public function export()
        {
            $items = $this->Items->find()->select([
                "id", "name", "price", "quantity", "category_id", "created", "modified"
            ])->all();
            $_serialize = 'items';
            $this->response->download('Items.csv'); // <= setting the file name
            $this->viewBuilder()->className('CsvView.Csv');
            $this->set(compact('items', '_serialize'));
        }
    }

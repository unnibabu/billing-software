<?php
    echo $this->Html->script("jquery.min.js");
    echo $this->Html->script("bootstrap.js");
    echo $this->Html->script("jQuery.print.js");
    echo $this->Html->css("select2.min.css");
    echo $this->Html->script("select2.full.min.js");
    
    echo $this->Html->css("jquery-ui.css");
    echo $this->Html->script("jquery-ui.js");
?>

<style>
    body {
        overflow-x: hidden;
    }

    td {
        font-size: 20px;
    }

    .ratevat {
        font-size: 20px;
    }

    input {
        height: 40px !important;
        padding: 0px;
        font-size: 23px !important;
    }

    input:focus {
        border: 2px solid grey;
    }

    body {
        counter-reset: section;
    }

    .counter::before {
        counter-increment: section;
        content: counter(section);
    }

    .counter {
        font-size: 24px;
    }

    .pricehere {
        font-size: 24px;
    }

    .selection {
        font-size: 24px;
    }

    #loadergif {
        display: none;
    }

    #onlyprint {
        /*display: none;*/
    }

    @media print {
        * {
            border: none !important;
            font-family: Arial !important;
        }

        .selection {
            border: none !important;
        }

        .top-bar {
            display: none;
        }

        input {
            height: 33px !important;
            padding: 0px;
        }

        .deleteme {
            display: none;
        }

        #loadergif {
            display: none;
        }

        .btn {
            display: none;
        }

        .table {
            width: 600px !important;
            margin-top: -70px;
        }

        td, th {
            font-size: 14px !important;
            font-weight: lighter !important;
        }

        .pricehere, .counter, .selection, .qtyitems, #billiddiv {
            font-size: 15px !important;
        }

        #shoptitle {
            width: 550px;
            font-size: 20px;
        }

        #totalprice {
            left: 0px;
            float: left !important;
            font-size: 15px !important;
            font-weight: normal !important;
        }

        td {
            overflow: hidden;
        }

        .noneed {
            display: none;
        }
    }

</style>
<div class="row" style="margin: 0px;padding: 0px;width: 100vw !important;max-width:none;">
    <div class="col-md-12" id="billgrid">

        <h3 id="shoptitle" style="text-align: center;color:grey;">PALAKUNNEL TRADERS</h3>
        <h5 id="billiddiv">Bill ID : <span id="billnumberid"><?php echo time() . rand(10, 99); ?></span></h5>
        <br><br>
        <table class="table">
            <thead>
            <tr>
                <th style="width: 10%;">No</th>
                <th style="width: 40%;">Item</th>
                <th>QTY</th>
                <th id="ratevatheader">Rate+vat</th>
                <th>Price
                    <?php
                        echo $this->Html->image("/img/loading.gif", ["id" => "loadergif"]);
                    ?>
                </th>
                <th style="width: 60px;" class="noneed"></th>
            </tr>
            </thead>
            <tbody id="appendbody">
            <tr id="tr1">
                <td class="counter"></td>
                <td>
                    <input onchange="getprice(this)" type="text" class="itemselect" id="selectitem1"/>
                    <input type="text" class="itemprice" id="itemprice1"/>
                </td>
                <td><input class="qtyitems" id="qty1" onchange="getprice(this)" type="text" placeholder="Item..."
                           tabindex="2"/></td>
                <td class="ratevat">

                </td>
                <td class="pricehere"></td>
                <td class="noneed">

                </td>
            </tr>

            </tbody>


        </table>

        <h3 class="pull-right" id="totalprice">Total : </h3>

    </div>
</div>

<br><br>
<div class="row">
    <div class="col-md-6">
        <button class="btn btn-lg btn-success pull-right" onclick="printme()">
            <span class="glyphicon glyphicon-print"></span>
            PRINT
        </button>
    </div>
    <div class="col-md-6">
        <button id="newbillbutton" class="btn btn-lg btn-primary" onclick="newbill()">
            <span class="glyphicon  glyphicon-pencil"></span>
            NEW BILL
        </button>
    </div>
</div>

<div id="onlyprint">
    <h4>PALAKUNNEL TRADERS</h4>
    <span id="billidprintdisplay"></span><br>
    <span id="datebilldisplay"></span>
    <div>
        <table id="printtable">
            <thead>
            <th>No</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Rate+VAT</th>
            <th>Price</th>
            </thead>
            <tbody id="printbody">

            </tbody>
        </table>
    </div>
    <span id="totalprinterdisplay"></span>
</div>


<br><br><br>
<script>
    var allitems = "<?php echo $allitems; ?>";

    var source = '<?php echo json_encode($testbill); ?>';
    var jsondata = JSON.parse(source);
    console.log(jsondata);
    //    $("#selectitem1").html(allitems);  //initial set selectbox1


    $('body').bind('keydown', function (e) {
        if (e.keyCode == 107) {
            e.preventDefault();
            append();
        }
    });


    var index = 1;
    function append() {
        index++;
        var appender = '<tr>' +
            '<td class="counter"></td>' +
            '<td>' +
            '<input type="text" onchange="getprice(this)" class="itemselect" id="selectitem' + index + '"/>' +
            '<input type="hidden"  class="itemprice" id="itemprice' + index + '"/>' +
            '</td>' +
            '<td><input class="qtyitems" onchange="getprice(this)" id="qty' + index + '" type="text" placeholder="Item..."/></td>' +
            '<td class="ratevat"></td>' +
            '<td class="pricehere"></td>' +
            '<td class="noneed"><button tabindex="-1" class="btn btn-danger deleteme">' +
            '<span class="glyphicon glyphicon-remove"></span>' +
            '</button>' +
            '</td>' +
            '</tr>';

        $("#appendbody").append(appender);
        $('html,body').animate({
            scrollTop: $(window).scrollTop() + 100
        })
        $("#selectitem" + index).autocomplete({
            minLength: 2,
            source: jsondata,
            focus: function (event, ui) {
                $("#selectitem" + index).val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $(this).parent().parent().find(".ratevat").html(ui.item.price);
                $(this).parent().parent().find(".qtyitems").focus();
                $(this).parent().parent().find(".itemprice").val(ui.item.id);
                return true;
            }
        });
        $("#selectitem" + index).focus();
    }


    $('body').on('click', '.deleteme', function () {
        $(this).parent().parent().remove();
        getTotalPrice();
    });


    $("#selectitem1").autocomplete({
        minLength: 4,
        source: jsondata,
        focus: function (event, ui) {
            $("#selectitem1").val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $(this).parent().parent().find(".ratevat").html(ui.item.price);
            $(this).parent().parent().find(".qtyitems").focus();
            $(this).parent().parent().find(".itemprice").val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            console.log(ui.item);
        }
    });


    var ajaxpriceLink = "<?php
        echo $this->Url->build([
            "controller" => "Items",
            "action"     => "getPrice"
        ]);
        ?>";


    function getprice(element) {
        var itemid = $(element).parent().parent().find(".itemprice").val();
        var quantity = $(element).parent().parent().find(".qtyitems").val();
        console.log("item id = " + itemid + " and qty =  " + quantity);

        if (itemid.length > 0 && quantity.length > 0) {
            $("#loadergif").css({"display": "inline-block"});
//            $.ajax({
//                method: "POST",
//                url: ajaxpriceLink,
//                data: {
//                    itemid: itemid,
//                    qty: quantity
//                }
//            }).done(function (msg) {
//                $(element).parent().parent().find(".pricehere").html(msg);
//                getTotalPrice();
//            });


            for (i = 0; i < jsondata.length; i++) {
                if (jsondata[i].id == itemid) {
                    $(element).parent().parent().find(".ratevat").html((jsondata[i].price));
                    $(element).parent().parent().find(".pricehere").html((jsondata[i].price * quantity).toFixed(2));
                    getTotalPrice();
                    break;
                }
            }

        }
    }


    function getTotalPrice() {
        var totalprice = 0;
        $(".pricehere").each(function () {
            totalprice += parseFloat($(this).html());
        });
        $("#totalprice").html("Total " + totalprice.toFixed(2));
        $("#loadergif").css({"display": "none"});
    }


    function printme() {
        var counter = 1;
        var printhtml = "";
        $(".itemselect ").each(function () {
            printhtml += "<tr>";
            printhtml += "<td>" + counter + "</td>";

            printhtml += "<td>" + $(this).val() + "</td>";
            printhtml += "<td>" + $(this).parent().parent().find(".qtyitems").val() + "</td>";
            printhtml += "<td>" + $(this).parent().parent().find(".ratevat").html() + "</td>";
            printhtml += "<td>" + $(this).parent().parent().find(".pricehere").html() + "</td>";
            printhtml += "</tr>";
            counter++;
        });

        $("#printbody").html(printhtml);
        $("#totalprinterdisplay").html($("#totalprice").text());
        $("#billidprintdisplay").html("Bill ID " + $("#billnumberid").text());
        var billdate = new Date();
        $("#datebilldisplay").html(billdate.getDate() + "/" + (billdate.getMonth() + 1) + "/" + billdate.getFullYear() + " " + billdate.getHours() + ":" + billdate.getMinutes() + ":" + billdate.getSeconds());
        $("#onlyprint").print({
            globalStyles: false,
            mediaPrint: false,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 750,
            title: null,
            doctype: '<!doctype html>'
        });
    }

    var addBillLink = "<?php
        echo $this->Url->build([
            "controller" => "Bills",
            "action"     => "add"
        ]);
        ?>";
    function newbill() {
        $("#newbillbutton").attr("disabled", true);
        $("#newbillbutton").html("Please Wait...");
        var billarray = [];
        $(".itemselect").each(function () {
            var billdata = {};
            billdata.billid = $("#billnumberid").html();
            billdata.item_id = $(this).val();
            billdata.quantity = $(this).parent().parent().find(".qtyitems").val();
            billdata.totalprice = $(this).parent().parent().find(".pricehere").html();
            billarray.push(billdata);
        });

        $.ajax({
            method: "POST",
            url: addBillLink,
            data: {
                item: billarray
            }
        }).done(function (msg) {
            parent.self.location = addBillLink;
        });
    }

    $(document).ajaxError(function () {
        $("#newbillbutton").attr("disabled", false);
        $("#newbillbutton").html("Try Again");
    });
</script>

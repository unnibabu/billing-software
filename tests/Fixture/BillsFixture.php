<?php
    namespace App\Test\Fixture;
    
    use Cake\TestSuite\Fixture\TestFixture;
    
    /**
     * BillsFixture
     
     */
    class BillsFixture extends TestFixture
    {
        
        /**
         * Fields
         * @var array
         */
        // @codingStandardsIgnoreStart
        public $fields = [
            'id'           => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
            'billid'       => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
            'item_id'      => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
            'quantity'     => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
            'totalprice'   => ['type' => 'decimal', 'length' => 9, 'precision' => 2, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => ''],
            'created'      => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
            'modified'     => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
            '_indexes'     => [
                'fkbillsitems' => ['type' => 'index', 'columns' => ['item_id'], 'length' => []],
            ],
            '_constraints' => [
                'primary'      => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
                'fkbillsitems' => ['type' => 'foreign', 'columns' => ['item_id'], 'references' => ['items', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            ],
            '_options'     => [
                'engine'    => 'InnoDB',
                'collation' => 'latin1_swedish_ci'
            ],
        ];
        // @codingStandardsIgnoreEnd
        
        /**
         * Records
         * @var array
         */
        public $records = [
            [
                'id'         => 1,
                'billid'     => 'Lorem ipsum dolor sit amet',
                'item_id'    => 1,
                'quantity'   => 1,
                'totalprice' => 1.5,
                'created'    => '2017-03-31 15:15:41',
                'modified'   => '2017-03-31 15:15:41'
            ],
        ];
    }

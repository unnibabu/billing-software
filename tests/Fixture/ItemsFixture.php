<?php
    namespace App\Test\Fixture;
    
    use Cake\TestSuite\Fixture\TestFixture;
    
    /**
     * ItemsFixture
     
     */
    class ItemsFixture extends TestFixture
    {
        
        /**
         * Fields
         * @var array
         */
        // @codingStandardsIgnoreStart
        public $fields = [
            'id'           => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
            'name'         => ['type' => 'string', 'length' => 500, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
            'price'        => ['type' => 'decimal', 'length' => 9, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
            'quantity'     => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
            'category_id'  => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
            'created'      => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
            'modified'     => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
            '_indexes'     => [
                'fkcategoryitems' => ['type' => 'index', 'columns' => ['category_id'], 'length' => []],
            ],
            '_constraints' => [
                'primary'         => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
                'fkcategoryitems' => ['type' => 'foreign', 'columns' => ['category_id'], 'references' => ['categories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            ],
            '_options'     => [
                'engine'    => 'InnoDB',
                'collation' => 'latin1_swedish_ci'
            ],
        ];
        // @codingStandardsIgnoreEnd
        
        /**
         * Records
         * @var array
         */
        public $records = [
            [
                'id'          => 1,
                'name'        => 'Lorem ipsum dolor sit amet',
                'price'       => 1.5,
                'quantity'    => 1,
                'category_id' => 1,
                'created'     => '2017-03-31 11:53:35',
                'modified'    => '2017-03-31 11:53:35'
            ],
        ];
    }
